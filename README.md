https://www.linkedin.com/pulse/create-react-app-without-create-react-app-cra-elhousieny-phd%E1%B4%AC%E1%B4%AE%E1%B4%B0

1. Inside a new folder, initialize yarn : `yarn init`
2. Create src folder with the files : index.js, bootstrap.js, App.js
3. Create public folder with the file : index.html
4. Add React to the project : `yarn add react react-dom --save`
5. Add webpack to the project : `yarn add webpack`
6. Add webpack server : `yarn add webpack-server webpack-cli webpack-dev-server`
7. Create webpack.config.js
    - Add HTML webpack plugin : `yarn add html-webpack-plugin`
    - Add Babel to understand JSX : `yarn add @babel/core @babel/plugin-transform-runtime @babel/preset-env @babel/preset-react babel-loader --save`
    - Add CSS-loader to translate CSS : `yarn add css-loader --save`
    - Add Style loader to inject styles : `yarn add style-loader --save`
8. Add SASS to the project : `yarn add sass-loader node-sass --save`
9. Run `yarn webpack` generate the webpack build
10. Run `webpack serve` to serve the webpack build in live development server.

Complete initialization command in case fresh project : `yarn init && yarn add react react-dom webpack webpack-server webpack-cli html-webpack-plugin @babel/core @babel/plugin-transform-runtime @babel/preset-env @babel/preset-react babel-loader css-loader style-loader sass-loader node-sass --save && yarn add webpack-dev-server --save-dev`

If package is already in place, just run `yarn` to install the dependencies.

Run `yarn run start` to start the project.
